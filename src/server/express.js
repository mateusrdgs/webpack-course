import express from 'express';
import path from 'path';

const server = express();
const port = 8080;

const webpack = require('webpack'),
	config = require('../../config/webpack.dev'),
	compiler = webpack(config);

const webpackDevMiddleware = require('webpack-dev-middleware')(compiler, config.devServer);

const webpackHotMiddleware = require('webpack-hot-middleware')(compiler)

server.use(webpackDevMiddleware);
server.use(webpackHotMiddleware);

const staticMiddleware = express.static('dist');

server.use(staticMiddleware);

server.listen(port, () => {
	console.log(`Server listening on port ${port}`);
});